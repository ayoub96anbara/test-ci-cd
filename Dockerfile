FROM adoptopenjdk/openjdk8
ADD target/spring-rest-api-test.jar spring-rest-api-test.jar
MAINTAINER anbara.ayoub@gmail.com
EXPOSE 8089
ENTRYPOINT ["java","-jar","spring-rest-api-test.jar"]
