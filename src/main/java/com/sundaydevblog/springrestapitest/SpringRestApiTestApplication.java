package com.sundaydevblog.springrestapitest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestApiTestApplication implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(SpringRestApiTestApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("NAME_STATIC: "+NAME_STATIC);
    }

    private static String NAME_STATIC;

    @Value("${s.name}")
    public void setNameStatic(String name){
       NAME_STATIC = name;
    }



}
